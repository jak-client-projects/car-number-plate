jQuery(function($){
	$(document).on('keyup','#registration',function(e){
		var text = $(this).val();
		if (text.length > 0 ) {
			$('.cnp-preview-area-1 span, .cnp-preview-area-2').html(text)
		}else{
			$('.cnp-preview-area-1 span, .cnp-preview-area-2').html('YOUR REG')
		}
	});
	$(document).on('keyup','#footer-text',function(e){
		var text = $(this).val();
		$('#cnp-footer-preview-panel').html(text)
	});
	$(document).on('click','.cnp-plate-type',function(e){
		var text = $(this).data('type');
		$('.cnp-plate-type').removeClass('active')
		$(this).addClass('active')
		$('#cnp-plate-type-input').val(text)
	});
	$(document).on('click','.cnp-step-btn',function(e){
		e.preventDefault();
		var step = $(this).data('step');
		$('.cnp-step').slideUp()

		$('#cnp-step-'+step).slideDown()
	});

	$(document).on('submit', '#cnp-form', function(e) {
		e.preventDefault();
		var data = $(this).serializeArray();
		$('.cnp-loader').show()
		$.ajax({
			url: CNP.ajaxurl,
			data: data,
			type: 'POST',
			dataType: 'JSON',
			success: function(resp){
				$('.cnp-loader').hide()
				console.log(resp);
				if (resp.status == 1) {
					window.location.href = resp.checkout_url;
				}
			}
		});
	})
})