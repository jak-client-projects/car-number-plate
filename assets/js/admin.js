jQuery(function($){
	$('.car-number-plate-help-heading').click(function(e){
		var $this = $(this);
		var target = $this.data('target');
		$('.car-number-plate-help-text:not('+target+')').slideUp();
		if($(target).is(':hidden')){
			$(target).slideDown();
		}
		else {
			$(target).slideUp();
		}
	});

	$('.cnp_help_tablinks .cnp_help_tablink').on( 'click', function(e){
        e.preventDefault();
        var tab_id = $(this).attr('id');
        $('.cnp_help_tablink').removeClass('active');
        $(this).addClass('active');

        $('.cnp_tabcontent').hide();
        $('#'+tab_id+'_content').show();
    } );
})