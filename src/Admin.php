<?php
/**
 * All admin facing functions
 */
namespace Codexpert\Car_Number_Plate;
use Codexpert\Plugin\Base;
use Codexpert\Plugin\Wizard;
use Codexpert\Plugin\Metabox;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage Admin
 * @author Codexpert <hi@codexpert.io>
 */
class Admin extends Base {

	public $plugin;

	/**
	 * Constructor function
	 */
	public function __construct( $plugin ) {
		$this->plugin	= $plugin;
		$this->slug		= $this->plugin['TextDomain'];
		$this->name		= $this->plugin['Name'];
		$this->server	= $this->plugin['server'];
		$this->version	= $this->plugin['Version'];
	}

	/**
	 * Internationalization
	 */
	public function i18n() {
		load_plugin_textdomain( 'car-number-plate', false, CNP_DIR . '/languages/' );
	}

	/**
	 * Installer. Runs once when the plugin in activated.
	 *
	 * @since 1.0
	 */
	public function install() {
		/**
		 * Schedule an event to sync help docs
		 */
		if ( !wp_next_scheduled ( 'codexpert-daily' )) {
		    wp_schedule_event( time(), 'daily', 'codexpert-daily' );
		}

		if( !get_option( 'car-number-plate_version' ) ){
			update_option( 'car-number-plate_version', $this->version );
		}
		
		if( !get_option( 'car-number-plate_install_time' ) ){
			update_option( 'car-number-plate_install_time', time() );
		}

		if( get_option( 'car-number-plate-docs-json' ) == '' ) {
			$this->daily();
		}
	}

	/**
	 * Uninstaller. Runs once when the plugin in deactivated.
	 *
	 * @since 1.0
	 */
	public function uninstall() {
		/**
		 * Remove scheduled hooks
		 */
		wp_clear_scheduled_hook( 'codexpert-daily' );
	}

	/**
	 * Daily events
	 */
	public function daily() {
		/**
		 * Sync docs from https://help.codexpert.io
		 *
		 * @since 1.0
		 */
	    if( isset( $this->plugin['doc_id'] ) && !is_wp_error( $_docs_data = wp_remote_get( "https://help.codexpert.io/wp-json/wp/v2/docs/?parent={$this->plugin['doc_id']}&per_page=20" ) ) ) {
	        update_option( 'car-number-plate-docs-json', json_decode( $_docs_data['body'], true ) );
	    }
	    
	}

	/**
	 * Enqueue JavaScripts and stylesheets
	 */
	public function enqueue_scripts() {
		$min = defined( 'CNP_DEBUG' ) && CNP_DEBUG ? '' : '.min';
		
		wp_enqueue_style( $this->slug, plugins_url( "/assets/css/admin{$min}.css", CNP ), '', $this->version, 'all' );

		wp_enqueue_script( $this->slug, plugins_url( "/assets/js/admin{$min}.js", CNP ), [ 'jquery' ], $this->version, true );

	}
}