<?php
/**
 * All public facing functions
 */
namespace Codexpert\Car_Number_Plate;
use Codexpert\Plugin\Base;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage Front
 * @author Codexpert <hi@codexpert.io>
 */
class Front extends Base {

	public $plugin;

	/**
	 * Constructor function
	 */
	public function __construct( $plugin ) {
		$this->plugin	= $plugin;
		$this->slug		= $this->plugin['TextDomain'];
		$this->name		= $this->plugin['Name'];
		$this->version	= $this->plugin['Version'];
	}

	public function add_admin_bar( $admin_bar ) {
		if( !current_user_can( 'manage_options' ) ) return;

		$admin_bar->add_menu( [
			'id'    => $this->slug,
			'title' => $this->name,
			'href'  => add_query_arg( 'page', $this->slug, admin_url( 'admin.php' ) ),
			'meta'  => [
				'title' => $this->name,            
			],
		] );
	}

	public function head() {}
	
	/**
	 * Enqueue JavaScripts and stylesheets
	 */
	public function enqueue_scripts() {
		$min = defined( 'CNP_DEBUG' ) && CNP_DEBUG ? '' : '.min';

		wp_enqueue_style( 'bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css' );

		wp_enqueue_style( $this->slug, plugins_url( "/assets/css/front{$min}.css", CNP ), '', $this->version, 'all' );

		wp_enqueue_script( $this->slug, plugins_url( "/assets/js/front{$min}.js", CNP ), [ 'jquery' ], $this->version, true );
		
		$localized = [
			'ajaxurl'	=> admin_url( 'admin-ajax.php' )
		];
		wp_localize_script( $this->slug, 'CNP', apply_filters( "{$this->slug}-localized", $localized ) );
	}

	public function display_meta_data( $cart_data, $cart_item )	{

		// if ( isset( $cart_item['neon_text'] ) ) {			
			$attr_keys 	= Helper::cnp_attrs();
			$colors 	= Helper::holder_colors( 'keys' );
			$_value 	= '';
			// unset( $cart_item['price'] );
			foreach ( $cart_item as $key => $value ) {
				if ( in_array( $key, $attr_keys ) ) {
					$cart_data[] = [
						'key' 	=> ucwords( str_replace( '_', ' ', $key ) ),
						'value' => $value,
					];
				}
				else if ( in_array( $key, $colors ) ) {
					$cart_data[] = [
						'key' 	=> ucwords( str_replace( '_', ' ', $key ) ). ' Holder',
						'value' => $value,
					];
				}
			}
			
		// }

		return $cart_data;
	}

	public function save_cnp_options( $item, $cart_item_key, $values, $order ) {

		$attr_keys 	= Helper::cnp_attrs();
		$colors 	= Helper::holder_colors( 'keys' );
		$_value 	= '';
		
		foreach ( $attr_keys as $key ) {
			if ( isset( $values[ $key ] ) && isset( $values[ $key ] ) ) {
				$_key = ucwords( str_replace( '_', ' ', $key ) );
			    $item->update_meta_data( $_key, $values[ $key ] );
			}
		}

		foreach ( $colors as $key ) {
			if ( isset( $values[ $key ] ) && isset( $values[ $key ] ) ) {
				$_key = ucwords( str_replace( '_', ' ', $key ) ) . ' Holder';
			    $item->update_meta_data( $_key, $values[ $key ] );
			}
		}

	}
}