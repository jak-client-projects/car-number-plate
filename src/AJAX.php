<?php
/**
 * All AJAX related functions
 */
namespace Codexpert\Car_Number_Plate;
use Codexpert\Plugin\Base;

/**
 * if accessed directly, exit.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @package Plugin
 * @subpackage AJAX
 * @author Codexpert <hi@codexpert.io>
 */
class AJAX extends Base {

	public $plugin;

	/**
	 * Constructor function
	 */
	public function __construct( $plugin ) {
		$this->plugin	= $plugin;
		$this->slug		= $this->plugin['TextDomain'];
		$this->name		= $this->plugin['Name'];
		$this->version	= $this->plugin['Version'];
	}

	public function name_plate_manager(){
		$response = [ 'status' => 0, 'message' => __( 'Unauthorized', 'car-number-plate' ) ];

		if( !wp_verify_nonce( $_POST['_wpnonce'] ) ){
			wp_send_json( $response );
		}

		$defaults = [ '_wp_http_referer', '_wpnonce', 'action' ];

		$product_id = Helper::get_option( 'car-number-plate_basic', 'cnp-product' );

		$attributes 	= [];
		$cnp_attrs 		= Helper::cnp_attrs();
		$cart_object 	= WC()->cart;
		// $price 			= sanitize_text_field( $_POST['price'] );

		foreach ( $_POST as $key => $value ) {
			if ( in_array( $key, $cnp_attrs ) ) {
				if ( $value != '' ) {
					$attributes[ $key ] = $value;
				}
			}
		}
		if ( isset( $_POST['color'] ) && count( $_POST['color'] ) > 0 ) {
			$colors = Helper::holder_colors();
			foreach ( $_POST['color'] as $key => $value ) {
				if ( $value != '' ) {
					$color 				= $colors[$key];
					$attributes[ $key ] = "<span class='cnp-color-block' style='background:{$color}'></span> x {$value}";
				}
			}
		}
		foreach ( $cart_object->get_cart() as $cart_item_key => $cart_item ) {
		     if ( $cart_item['product_id'] == $product_id ) {
		          $cart_object->remove_cart_item( $cart_item_key );
		     }
		}
		$cart_object->add_to_cart( $product_id , 1,  0, [], $attributes );
		
		// foreach ( $cart_object->get_cart() as $cart_item_key => $cart_item ) {
		//      if ( $cart_item['product_id'] == $product_id ) {
		//            $cart_item['data']->set_price( $price );
		//      }
		// }
		$response['status'] 		= 1;
		$response['checkout_url'] 	= wc_get_checkout_url();
		$response['message'] 		= __( 'Product Added to cart', 'neon-product' );
		wp_send_json( $response );

		$response['_POST'] = $_POST;
		$response['attributes'] = $attributes;
		wp_send_json( $response );
	}

}