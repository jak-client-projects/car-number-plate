<?php 
use Codexpert\Car_Number_Plate\Helper;
 ?>
<div id="car-number-plate-panel">
	<div class="cnp-loader"><div class="loader"></div></div>
	<div class="container">
		<!-- <div class="cnp-top-bar text-center">
			<?php _e( '25% OFF All Number Plates - Same Day Dispatch before 4PM - New September 2021 Road Legal Badges Now Live!', 'car-number-plate' ) ?>
		</div>
		<div class="bg-skyblue text-center">
			<div class="row">
				<div class="col-md-3 col-sm-6"><?php _e( '14 Day Money Back Guarantee', 'car-number-plate' ) ?></div>
				<div class="col-md-3 col-sm-6"><?php _e( '3 Year Warranty', 'car-number-plate' ) ?></div>
				<div class="col-md-3 col-sm-6"><?php _e( 'Quality Guaranteed', 'car-number-plate' ) ?></div>
				<div class="col-md-3 col-sm-6"><?php _e( 'No Documents Required', 'car-number-plate' ) ?></div>
			</div>
		</div> -->

		<form class="my-4" id="cnp-form" action="">
			<div id="cnp-step-1" class="cnp-body-content cnp-step">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="cnp-left-area">
							<ul>
								<li class="cnp-button-title"><?php _e( 'Acrylic Options', 'car-number-plate' ); ?></li>
								<li><button class="cnp-plate-type active" data-type="standard"><?php _e( 'Standard', 'car-number-plate' ); ?></button></li>
								<li><button class="cnp-plate-type" data-type="black-gel"><?php _e( 'Black Gel', 'car-number-plate' ); ?></button></li>
								<li><button class="cnp-plate-type" data-type="carbon-gel"><?php _e( 'Carbon Gel', 'car-number-plate' ); ?></button></li>
								<li><button class="cnp-plate-type" data-type="laser-4d"><?php _e( 'Laser 4D', 'car-number-plate' ); ?></button></li>
								<li class="cnp-button-title"><?php _e( 'Metal Option', 'car-number-plate' ); ?></li>
								<li><button class="cnp-plate-type" data-type="standard-m"><?php _e( 'Standard', 'car-number-plate' ); ?></button></li>
								<li><button class="cnp-plate-type" data-type="vintage"><?php _e( 'Vintage', 'car-number-plate' ); ?></button></li>
							</ul>
						</div>
					</div>
					<div class="col-md-9 col-sm-12">
						<div class="cnp-right-area">
							<div class="cnp-form-area">
								<input type="hidden" name="action" value="cnp-form">
								<input id="cnp-plate-type-input" type="hidden" name="plate_type" value="standard">
								<?php wp_nonce_field() ?>
								<div class="cnp-form-area">
									<div class="row">
										<div class="col-md-6">
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Registration', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<input type="text" class="form-control" id="registration" name="registration" placeholder="<?php _e( 'Your Reg', 'car-number-plate' ); ?>">
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Font Size', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="font_size" name="font_size">
												      	<option value="152x102mm(6x4in)"><?php _e( '152x102mm(6x4in)', 'car-number-plate' ); ?></option>
												      	<option value="178x127mm(7x5in)"><?php _e( '178x127mm(7x5in)', 'car-number-plate' ); ?></option>
												      	<option value="178x152mm(7x6in)"><?php _e( '178x152mm(7x6in)', 'car-number-plate' ); ?></option>
												      	<option value="203x152mm(8x6in)"><?php _e( '203x152mm(8x6in)', 'car-number-plate' ); ?></option>
												      	<option value="229x178mm(9x7in)"><?php _e( '229x178mm(9x7in)', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Rear Size', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="rear_size" name="rear_size">
												      	<option value="152x102mm(6x4in)"><?php _e( '152x102mm(6x4in)', 'car-number-plate' ); ?></option>
												      	<option value="178x127mm(7x5in)"><?php _e( '178x127mm(7x5in)', 'car-number-plate' ); ?></option>
												      	<option value="178x152mm(7x6in)"><?php _e( '178x152mm(7x6in)', 'car-number-plate' ); ?></option>
												      	<option value="203x152mm(8x6in)"><?php _e( '203x152mm(8x6in)', 'car-number-plate' ); ?></option>
												      	<option value="229x178mm(9x7in)"><?php _e( '229x178mm(9x7in)', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="footer-text" class="col-sm-4 col-form-label"><?php _e( 'Footer Text', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<input type="text" class="form-control" id="footer-text" name="footer_text" placeholder="">
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Footer Font', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="footer_font" name="footer_font">
												      	<option value="verdana"><?php _e( 'Verdana', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Footer Colour', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="footer_color" name="footer_color">
												      	<option value="black"><?php _e( 'Black', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										</div>
										<div class="col-md-6">
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Font', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="font" name="font">
												      	<option value="UK_Legal"><?php _e( 'UK Legal', 'car-number-plate' ); ?></option>
												      	<option value="UK_Legal_Motorcycle"><?php _e( 'UK Legal Motorcycle', 'car-number-plate' ); ?></option>
												      	<option value="UK_3D"><?php _e( 'UK 3D', 'car-number-plate' ); ?></option>
												      	<option value="UK_Highline"><?php _e( 'UK Highline', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Border', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="border" name="border">
												      	<option value="No_Border"><?php _e( 'No Border', 'car-number-plate' ); ?></option>
												      	<option value="aqua"><?php _e( 'Aqua', 'car-number-plate' ); ?></option>
												      	<option value="black"><?php _e( 'Black', 'car-number-plate' ); ?></option>
												      	<option value="blue"><?php _e( 'Blue', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Badge', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="badge" name="badge">
												      	<option value="No_Border"><?php _e( 'No Badge', 'car-number-plate' ); ?></option>
												      	<option value="No_Border"><?php _e( 'Transparent | GB', 'car-number-plate' ); ?></option>
												      	<option value="No_Border"><?php _e( 'Transparent | GB Flag', 'car-number-plate' ); ?></option>
												      	<option value="No_Border"><?php _e( 'Transparent | GB Isles', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Front QTY', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6"><input type="number" class="form-control" name="front_quantity" required="" min="1" step="1"value="1"></div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Rear QTY', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6"><input type="number" class="form-control" name="rear_quantity" required="" min="1" step="1"value="1"></div>
										  	</div>
										  	<div class="form-group row">
											    <label for="registration" class="col-sm-4 col-form-label"><?php _e( 'Legal Details', 'car-number-plate' ); ?></label>
											    <div class="col-sm-6">
											      	<select class="form-control" id="legal_details" name="legal_details">
												      	<option value="yes"><?php _e( 'Yes', 'car-number-plate' ); ?></option>
												      	<option value="no"><?php _e( 'no', 'car-number-plate' ); ?></option>
												    </select>
											    </div>
										  	</div>
										</div>
									</div>
								</div>
							</div>
							<div class="cnp-form-footer-button-area">
								<ul>
									<li><a href=""><?php _e( 'Update Design', 'car-number-plate' ); ?></a></li>
									<li><a href=""><?php _e( 'Reset Design', 'car-number-plate' ); ?></a></li>
									<li><a href="" class="cnp-step-btn" data-step='2'><?php _e( 'Buy Holder', 'car-number-plate' ); ?></a></li>
									<li><a href=""><?php _e( 'View Cart', 'car-number-plate' ); ?></a></li>
									<li><button type="submit"><?php _e( 'Add to Cart', 'car-number-plate' ); ?></button></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="cnp-preview-area">
							<div class="cnp-preview-panel cnp-preview-area-1 standard">
								<span>Your Reg</span>
								<div id="cnp-footer-preview-panel"></div>
							</div>
							<div class="cnp-preview-panel cnp-preview-area-2 standard">Your Reg
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="cnp-step-2" class="cnp-step">
				<?php 
					$holder_img = plugins_url( "/assets/img/holder.jpg", CNP );
					$holders = Helper::holder_colors();
				 ?>
				 <button class="cnp-step-btn" data-step='1'><?php _e( 'Back', 'car-number-plate' ) ?></button>
				 <h2 class="cnp-step-title"><?php _e( 'Buy Number Plate Holders', 'car-number-plate' ) ?></h2>
				<div class="row">
					<?php foreach ( $holders as $name => $color ): ?>
					<div class="col-md-4">
						<div class="form-group row d-flex align-items-center mb-4">
							<div class="col-sm-8 d-flex align-items-center"><?php echo strtoupper( str_replace( '_', ' ', $name ) ) ?><span class="cnp-color-block" style="background:<?php echo $color ?>"></span></div>
							<div class="col-sm-4">
								<input class="form-control" type="number" name="color[<?php echo $name ?>]" min="0">
							</div>
						</div>
					</div>						
					<?php endforeach; ?>
				</div>
				<div class="row">
					
					<div class="col-md-6 mt-4">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
					<div class="col-md-6">
						<img src="<?php echo $holder_img; ?>">
					</div>
				</div>
				<div class="">
					<button type="submit"><?php _e( 'Checkout', 'car-number-plate' ) ?></button>
				</div>
			</div>
		</form>
	</div>
</div>